/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Image from "@material-ui/icons/Image";
import RotateRight from "@material-ui/icons/RotateRight";
import Wallpaper from "@material-ui/icons/Wallpaper";
import Equalizer from "@material-ui/icons/Equalizer";
// core components/views for Admin layout
import BasicPage from "views/Basic/Basic.js";
import Rotation from "views/Rotation/Rotation.js";
import ImageFormat from "views/Format/Format.js";
import StatisticsPage from "views/Statistics/Statistics.js";

const dashboardRoutes = [
  {
    path: "/basic-image-manipulation",
    name: "Basic Image Manipulation",
    rtlName: "لوحة القيادة",
    icon: Image,
    component: BasicPage,
    layout: "/admin"
  },
  // Below is not supported atm
  /* {
    path: "/rotation",
    name: "Rotation",
    icon: RotateRight,
    component: Rotation,
    layout: "/admin"
  },
  {
    path: "/change-image-format",
    name: "Change Image Format",
    icon: Wallpaper,
    component: ImageFormat,
    layout: "/admin"
  }, */ 
  {
    path: "/stats",
    name: "Statistics",
    icon: Equalizer,
    component: StatisticsPage,
    layout: "/admin"
  },
];

export default dashboardRoutes;
