import React, {useState} from "react";
import Button from "components/CustomButtons/Button.js";
import Textfield from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.js";
import axios from 'axios';

import { makeStyles } from "@material-ui/core/styles";

import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function Rotation() {

  const [imageSelected, setImageSelected] = useState("");
  const [inputtedRotation, setInputtedRotation] = useState("");
  const [isFileSelected, setIsFileSelected] = useState("");
  const [isRotationFilled, setIsRotationFilled] = useState("");
  const [disableButton, setDisableButton] = useState(true);
  const classes = useStyles();

  var width, height; //to grab image dimensions

  const loadImage = () => { //load image from user and grab dimensions
    return new Promise((resolve) => {
      var reader = new FileReader();

      //Read the contents of Image File.
      reader.readAsDataURL(imageSelected);
      
      reader.onload = function (e) {
        //Initiate the JavaScript Image object.
        var image = new Image();
      
        //Set the Base64 string return from FileReader as source.
        image.src = e.target.result;
      
        image.onload = function () {
          height = this.height;
          width = this.width;
          resolve(image)
        };
      };
    })
  }

  const uploadImage = () => {
    
    loadImage().then(() => { //wait to fetch dimensions, before constructing post request
    
    const data = { //construct body of post request
      'size': imageSelected.size,
      'name': imageSelected.name,
      'type': imageSelected.type,
      'action': 'rotation',
      'arguments': inputtedRotation % 360,
      'dimensions': {width, height}
    }

    //need CORS support first. use url below for testing
    //https://jsonplaceholder.typicode.com/posts
    axios.post('http://localhost:8000/upload', data)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error)
      }
    )
    });
  }

  return (
    <Card>
      <CardHeader color="primary">
        <h4 className={classes.cardTitleWhite}>Rotation</h4>
        <p className={classes.cardCategoryWhite}>
          Rotates the image clockwise
        </p>
      </CardHeader>
      <CardBody>
        <Grid container>
          <GridItem xs={12} sm={12} md={4}>
            <Textfield
              label="Clockwise Rotation Degrees"
              variant='outlined'
              required
              id="float"
              type='number'
              helperText='Field required'
              fullWidth
              value={inputtedRotation}
              onChange={(event)=> {
                setInputtedRotation(event.target.value);
                setIsRotationFilled('true');
                if (isFileSelected === 'true') {
                  setDisableButton(false)
                }
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <input 
              type="file"
              accept="image/jpg, image/png, image/gif, image/webp"
              multiple={false} //single files
              onChange={(event)=> {
                setImageSelected(event.target.files[0]);
                setIsFileSelected('true');
                  if (isRotationFilled === 'true') {
                    setDisableButton(false)
                  }
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Button disabled={disableButton} color="primary" round onClick={uploadImage}>
              Upload Image
            </Button>
          </GridItem>
        </Grid>
      </CardBody>
    </Card>
  );
}


