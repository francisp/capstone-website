import React, {useState} from "react";
import Button from "components/CustomButtons/Button.js";
import Radio from "@material-ui/core/Radio";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";
import styles from "assets/jss/material-dashboard-react/checkboxAdnRadioStyle.js";
import axios from 'axios';

import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";

const cardStyles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const cardUseStyles = makeStyles(cardStyles);
const useStyles = makeStyles(styles);

export default function Format() {

  const [imageSelected, setImageSelected] = useState("");
  const [selectedFormat, setSelectedFormat] = useState("");
  const [isFileSelected, setIsFileSelected] = useState("");
  const [isFormatSelected, setIsFormatSelected] = useState("");
  const [disableButton, setDisableButton] = useState(true);
  const cardClasses = cardUseStyles();

  var width, height; //to grab image dimensions

  const loadImage = () => { //load image from user and grab dimensions
    return new Promise((resolve) => {
      var reader = new FileReader();

      //Read the contents of Image File.
      reader.readAsDataURL(imageSelected);
      
      reader.onload = function (e) {
        //Initiate the JavaScript Image object.
        var image = new Image();
      
        //Set the Base64 string return from FileReader as source.
        image.src = e.target.result;
      
        image.onload = function () {
          height = this.height;
          width = this.width;
          resolve(image)
        };
      };
    })
  }

  const uploadImage = () => {
    loadImage().then(() => { //wait to fetch dimensions, before constructing post request

    const data = { //construct body of post request
      'size': imageSelected.size,
      'name': imageSelected.name,
      'type': imageSelected.type,
      'action': 'change-format',
      'arguments': selectedFormat,
      'dimensions': {width, height}
    }

    //need CORS support first. use url below for testing
    //https://jsonplaceholder.typicode.com/posts
    axios.post('http://localhost:8000/upload', data)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error)
      }
    )
    });
  }

  const classes = useStyles();

  return (
    <Card>
      <CardHeader color="primary">
        <h4 className={cardClasses.cardTitleWhite}>Change Image Format</h4>
        <p className={cardClasses.cardCategoryWhite}>
          Can convert an image to a JPEG, PNG, GIF, or WEBP file
        </p>
      </CardHeader>
      <CardBody>
        <Grid container>
          <GridItem xs={12} sm={12} md={4}>
            <Radio
              checked={selectedFormat === "jpg"}
              onChange={() => {
                setSelectedFormat("jpg");
                setIsFormatSelected('true');
                if (isFileSelected === 'true') {
                  setDisableButton(false)
                }
              }}
              value="jpg"
              name="JPG"
              aria-label="A"
              icon={<FiberManualRecord className={classes.radioUnchecked} />}
              checkedIcon={<FiberManualRecord className={classes.radioChecked} />}
              classes={{
                checked: classes.radio
              }}
            />
            JPG
            <Radio
              checked={selectedFormat === "png"}
              onChange={() => {
                setSelectedFormat("png")
                setIsFormatSelected('true');
                if (isFileSelected === 'true') {
                  setDisableButton(false)
                }
              }}
              value="png"
              name="PNG"
              aria-label="B"
              icon={<FiberManualRecord className={classes.radioUnchecked} />}
              checkedIcon={<FiberManualRecord className={classes.radioChecked} />}
              classes={{
                checked: classes.radio
              }}
            />
            PNG
            <Radio
              checked={selectedFormat === "gif"}
              onChange={() => {
                setSelectedFormat("gif")
                setIsFormatSelected('true');
                if (isFileSelected === 'true') {
                  setDisableButton(false)
                }
              }}
              value="gif"
              name="GIF"
              aria-label="C"
              icon={<FiberManualRecord className={classes.radioUnchecked} />}
              checkedIcon={<FiberManualRecord className={classes.radioChecked} />}
              classes={{
                checked: classes.radio
              }}
            />
            GIF
            <Radio
              checked={selectedFormat === "webp"}
              onChange={() => {
                setSelectedFormat("webp")
                setIsFormatSelected('true');
                if (isFileSelected === 'true') {
                  setDisableButton(false)
                }
              }}
              value="webp"
              name="WEBP"
              aria-label="D"
              icon={<FiberManualRecord className={classes.radioUnchecked} />}
              checkedIcon={<FiberManualRecord className={classes.radioChecked} />}
              classes={{
                checked: classes.radio
              }}
            />
            WEBP
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <input 
              type="file"
              accept="image/jpg, image/png, image/gif, image/webp"
              multiple={false} //single files
              onChange={(event)=> {
                setImageSelected(event.target.files[0]);
                setIsFileSelected('true');
                if (isFormatSelected === 'true') {
                  setDisableButton(false)
                }
              }}
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Button disabled={disableButton} color="primary" round onClick={uploadImage}>
              Upload Image
            </Button>
          </GridItem>
        </Grid>
      </CardBody>
    </Card>
  );
}


