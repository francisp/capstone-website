export const userUtilizationChartConfig = {
    type: 'line',
    data: {
        xAxes: [{
            type: 'time',
            time: {
                displayFormats: {
                    millisecond: 'mm:ss:SSS'
                }
            },
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        yAxes: [{
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        datasets: [
            {
                label: 'Power consumption (uW)',
                data: [],
                backgroundColor: [
                    'rgb(75, 192, 192)',
                ],
                borderWidth: 4
            }
        ]
    },
};

export const userUtilizationChartConfigSpectre = {
    type: 'line',
    data: {
        xAxes: [{
            type: 'time',
            time: {
                displayFormats: {
                    millisecond: 'mm:ss:SSS'
                }
            },
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        yAxes: [{
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        datasets: [
            {
                label: 'Power consumption (uW)',
                data: [],
                backgroundColor: [
                    'rgb(75, 192, 192)',
                ],
                borderWidth: 4
            }
        ]
    },
};
