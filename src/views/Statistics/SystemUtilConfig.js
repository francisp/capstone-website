export const systemUtilChartConfig = {
    type: 'line',
    data: {
        xAxes: [{
            type: 'time',
            time: {
                displayFormats: {
                    millisecond: 'mm:ss:SSS'
                }
            },
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        yAxes: [{
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        datasets: [
            {
                label: 'System Utilization',
                data: [],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.6)'
                ],
                borderWidth: 4
            }
        ]
    },
    options: {
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true
                    }
                }
            ]
        }
    }
};

export const systemUtilChartConfigSpectre = {
    type: 'line',
    data: {
        xAxes: [{
            type: 'time',
            time: {
                displayFormats: {
                    millisecond: 'mm:ss:SSS'
                }
            },
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        yAxes: [{
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        datasets: [
            {
                label: 'System Utilization',
                data: [],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.6)'
                ],
                borderWidth: 4
            }
        ]
    },
    options: {
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true
                    }
                }
            ]
        }
    }
};
