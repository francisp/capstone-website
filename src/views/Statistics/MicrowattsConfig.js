export const microwattsChartConfig = {
    type: 'line',
    data: {
        xAxes: [{
            type: 'time',
            time: {
                displayFormats: {
                    millisecond: 'mm:ss:SSS'
                }
            },
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        yAxes: [{
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        datasets: [
            {
                label: 'Power consumption (uW)',
                data: [],
                backgroundColor: [
                    'rgb(75, 192, 192)',
                ],
                borderWidth: 4
            }
        ]
    },
};

export const microwattsChartConfigSpectre = {
    type: 'line',
    data: {
        xAxes: [{
            type: 'time',
            time: {
                displayFormats: {
                    millisecond: 'mm:ss:SSS'
                }
            },
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        yAxes: [{
            gridLines: {
                zeroLineColor: '#ffffff'
            }
        }],
        datasets: [
            {
                label: 'Power consumption (uW)',
                data: [],
                backgroundColor: [
                    'rgb(75, 192, 192)',
                ],
                borderWidth: 4
            }
        ]
    },
};
