/*eslint-disable*/
import React, { useState, useEffect, useRef } from "react";
import Chartjs from 'chart.js';
import { API_HOST } from '../../utils/api';
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import AddAlert from "@material-ui/icons/AddAlert";
// core components
import GridItem from "components/Grid/GridItem.js";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "components/CustomButtons/Button.js";

import { Line } from 'react-chartjs-2'
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Icon from "@material-ui/core/Icon";

import axios from 'axios';

import { systemUtilChartConfig, systemUtilChartConfigSpectre } from './SystemUtilConfig';

import AccessTime from "@material-ui/icons/AccessTime";
import ChartistGraph from "react-chartist";
import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";
import { microwattsChartConfig, microwattsChartConfigSpectre } from "./MicrowattsConfig";
import { microjoulesChartConfig, microjoulesChartConfigSpectre } from "./MicrojoulesConfig";
import { userUtilizationChartConfig, userUtilizationChartConfigSpectre } from "./UserUtilizationConfig";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function Statistics() {

  const classes = useStyles();
  const [systemChartData, setSystemChartData] = useState({})
  const [timeData, setTimeData] = useState([])

  const systemUtilChartRef = useRef(React.createRef())
  const [chartInstance, setChartInstance] = useState(null);
  const microwattsChartRef = useRef(React.createRef());
  const [microwattsChartInstance, setMicrowattsChartInstance] = useState(null);
  const microjoulesChartRef = useRef(React.createRef());
  const [microjoulesChartInstance, setMicrojoulesChartInstance] = useState(null);
  const userUtilizationChartRef = useRef(React.createRef());
  const [userUtilizationChartInstance, SetUserUtilizationChartInstance] = useState(null);
  const [totalMicroJoules, setTotalMicroJoules] = useState(0);

  const systemUtilChartRefSpectre = useRef(React.createRef())
  const [chartInstanceSpectre, setChartInstanceSpectre] = useState(null);
  const microwattsChartRefSpectre = useRef(React.createRef());
  const [microwattsChartInstanceSpectre, setMicrowattsChartInstanceSpectre] = useState(null);
  const microjoulesChartRefSpectre = useRef(React.createRef());
  const [microjoulesChartInstanceSpectre, setMicrojoulesChartInstanceSpectre] = useState(null);
  const userUtilizationChartRefSpectre = useRef(React.createRef());
  const [userUtilizationChartInstanceSpectre, SetUserUtilizationChartInstanceSpectre] = useState(null);
  const [totalMicroJoulesSpectre, setTotalMicroJouleSpectres] = useState(0);

  const allChartInstances = [microwattsChartInstance, microjoulesChartInstance, userUtilizationChartInstance, chartInstance,
    microwattsChartInstanceSpectre, microjoulesChartInstanceSpectre, userUtilizationChartInstanceSpectre, chartInstanceSpectre]

  const repeat = () => {
    setInterval(() => {
      update()
    }, 1000)
  }

  const updateChartData = (dataPoints) => {
    console.log(dataPoints);
    const labelArr = [];
    const microwattData = [];
    const microjoulesData = [];
    const userUtilizationData = [];
    const systemUtilizationData = [];

    let meltdownPoints = dataPoints.filter(points => points.nodeID.ipv4 == "128.173.89.246");
    let spectrePoints = dataPoints.filter(points => points.nodeID.ipv4 == "10.98.0.4");

    let meltdownLastPoint = meltdownPoints[meltdownPoints.length - 1];
    setTotalMicroJoules(meltdownLastPoint.cumulative_micro_joules);

    let spectreLastPoint = spectrePoints[spectrePoints.length - 1];
    setTotalMicroJouleSpectres(spectreLastPoint.cumulative_micro_joules);

    meltdownPoints.forEach(data => {
      // add label
      var today = new Date(data.time);
      var time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
      labelArr.push(time);

      // add data to respective array
      microwattData.push(data.micro_watts);
      microjoulesData.push(data.micro_joules);
      userUtilizationData.push(data.user_utilization);
      systemUtilizationData.push(data.system_utilization);
    });

    // update charts with the new data
    microwattsChartInstance.data.labels = labelArr;
    microwattsChartInstance.data.datasets.forEach(dataset => {
      dataset.data = microwattData;
    });

    microjoulesChartInstance.data.labels = labelArr;
    microjoulesChartInstance.data.datasets.forEach(dataset => {
      dataset.data = microjoulesData;
    });

    userUtilizationChartInstance.data.labels = labelArr;
    userUtilizationChartInstance.data.datasets.forEach(dataset => {
      dataset.data = userUtilizationData;
    });

    chartInstance.data.labels = labelArr;
    chartInstance.data.datasets.forEach(dataset => {
      dataset.data = systemUtilizationData;
    })


    const labelArrSpectre = [];
    const microwattDataSpectre = [];
    const microjoulesDataSpectre = [];
    const userUtilizationDataSpectre = [];
    const systemUtilizationDataSpectre = [];
    spectrePoints.forEach(data => {
      // add label
      var today = new Date(data.time);
      var time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
      labelArrSpectre.push(time);

      // add data to respective array
      microwattDataSpectre.push(data.micro_watts);
      microjoulesDataSpectre.push(data.micro_joules);
      userUtilizationDataSpectre.push(data.user_utilization);
      systemUtilizationDataSpectre.push(data.system_utilization);
    });

    // update charts with the new data
    microwattsChartInstanceSpectre.data.labels = labelArrSpectre;
    microwattsChartInstanceSpectre.data.datasets.forEach(dataset => {
      dataset.data = microwattDataSpectre;
    });

    microjoulesChartInstanceSpectre.data.labels = labelArrSpectre;
    microjoulesChartInstanceSpectre.data.datasets.forEach(dataset => {
      dataset.data = microjoulesDataSpectre;
    });

    userUtilizationChartInstanceSpectre.data.labels = labelArrSpectre;
    userUtilizationChartInstanceSpectre.data.datasets.forEach(dataset => {
      dataset.data = userUtilizationDataSpectre;
    });

    chartInstanceSpectre.data.labels = labelArrSpectre;
    chartInstanceSpectre.data.datasets.forEach(dataset => {
      dataset.data = systemUtilizationDataSpectre;
    })

    allChartInstances.forEach(chart => chart.update());
  }

  const update = () => {
    axios.get(`${API_HOST}/master/statistics`)
      .then(response => { updateChartData(response.data) })
      .catch(error => console.error(error));
  }

  useEffect(() => {
    if (systemUtilChartRef && systemUtilChartRef.current) {
      const newChartInstance = new Chartjs(systemUtilChartRef.current, systemUtilChartConfig);
      setChartInstance(newChartInstance);
    }

    if (microwattsChartRef && microwattsChartRef.current) {
      const newMicrowattChartInstance = new Chartjs(microwattsChartRef.current, microwattsChartConfig);
      setMicrowattsChartInstance(newMicrowattChartInstance);
    }

    if (microjoulesChartRef && microjoulesChartRef.current) {
      const newMicrojoulesChartInstance = new Chartjs(microjoulesChartRef.current, microjoulesChartConfig);
      setMicrojoulesChartInstance(newMicrojoulesChartInstance);
    }

    if (userUtilizationChartRef && userUtilizationChartRef.current) {
      const newUserUtilizationChartInstance = new Chartjs(userUtilizationChartRef.current, userUtilizationChartConfig);
      SetUserUtilizationChartInstance(newUserUtilizationChartInstance);
    }

    if (systemUtilChartRefSpectre && systemUtilChartRefSpectre.current) {
      const newChartInstance = new Chartjs(systemUtilChartRefSpectre.current, systemUtilChartConfigSpectre);
      setChartInstanceSpectre(newChartInstance);
    }

    if (microwattsChartRefSpectre && microwattsChartRefSpectre.current) {
      const newMicrowattChartInstance = new Chartjs(microwattsChartRefSpectre.current, microwattsChartConfigSpectre);
      setMicrowattsChartInstanceSpectre(newMicrowattChartInstance);
    }

    if (microjoulesChartRefSpectre && microjoulesChartRefSpectre.current) {
      const newMicrojoulesChartInstance = new Chartjs(microjoulesChartRefSpectre.current, microjoulesChartConfigSpectre);
      setMicrojoulesChartInstanceSpectre(newMicrojoulesChartInstance);
    }

    if (userUtilizationChartRefSpectre && userUtilizationChartRefSpectre.current) {
      const newUserUtilizationChartInstance = new Chartjs(userUtilizationChartRefSpectre.current, userUtilizationChartConfigSpectre);
      SetUserUtilizationChartInstanceSpectre(newUserUtilizationChartInstance);
    }
  }, [systemUtilChartRef, microwattsChartRef, microjoulesChartRef, userUtilizationChartRef,
    systemUtilChartRefSpectre, microwattsChartRefSpectre, microjoulesChartRefSpectre, userUtilizationChartRefSpectre])

  return (
    <Card>
      <CardHeader color="primary">
        <h4 className={classes.cardTitleWhite}>Statistics</h4>
        <Button color="primary" round onClick={repeat}> Start Updating</Button>
      </CardHeader>
      <CardBody>
        <Grid container>

          <GridItem md={6}>
            <Card>
              <CardHeader color="info">
                <h4 className={classes.cardTitleWhite}>Node 1</h4>
              </CardHeader>
              <Grid container>
                <GridItem xs={12} sm={12} md={6}>
                  <Card chart>
                    <CardHeader>
                      <canvas ref={microwattsChartRef} />
                    </CardHeader>
                    <CardFooter chart>
                      Microwatts
                    </CardFooter>
                  </Card>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Card chart>
                    <CardHeader>
                      <canvas ref={microjoulesChartRef} />
                    </CardHeader>
                    <CardFooter chart>
                      Microjoules
                    </CardFooter>
                  </Card>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Card chart>
                    <CardHeader>
                      <canvas ref={userUtilizationChartRef} />
                    </CardHeader>
                    <CardFooter chart>
                      User Utilization
                    </CardFooter>
                  </Card>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Card chart>
                    <CardHeader>
                      <canvas ref={systemUtilChartRef} />
                    </CardHeader>
                    <CardFooter chart>
                      System Utilization
                    </CardFooter>
                  </Card>
                </GridItem>
                <GridItem xs={3} sm={3} md={6}>
                  <Card>
                    <CardHeader color="warning" stats icon>
                      <CardIcon color="warning">
                        <Icon>bolt</Icon>
                      </CardIcon>
                      <h3 className={classes.cardTitle}>
                        dsadsadsa
                      </h3>
                    </CardHeader>
                    <CardBody >
                      Total Microjoules: {totalMicroJoules}
                    </CardBody>
                  </Card>
                </GridItem>
              </Grid>
            </Card>
          </GridItem>

          <GridItem md={6}>
            <Card>
              <CardHeader color="info">
                <h4 className={classes.cardTitleWhite}>Node 2</h4>
              </CardHeader>
              <Grid container>
                <GridItem xs={12} sm={12} md={6}>
                  <Card chart>
                    <CardHeader>
                      <canvas ref={microwattsChartRefSpectre} />
                    </CardHeader>
                    <CardFooter chart>
                      Microwatts
                    </CardFooter>
                  </Card>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Card chart>
                    <CardHeader>
                      <canvas ref={microjoulesChartRefSpectre} />
                    </CardHeader>
                    <CardFooter chart>
                      Microjoules
                    </CardFooter>
                  </Card>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Card chart>
                    <CardHeader>
                      <canvas ref={userUtilizationChartRefSpectre} />
                    </CardHeader>
                    <CardFooter chart>
                      User Utilization
                    </CardFooter>
                  </Card>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <Card chart>
                    <CardHeader>
                      <canvas ref={systemUtilChartRefSpectre} />
                    </CardHeader>
                    <CardFooter chart>
                      System Utilization
                    </CardFooter>
                  </Card>
                </GridItem>
                <GridItem xs={3} sm={3} md={6}>
                  <Card>
                    <CardHeader color="warning" stats icon>
                      <CardIcon color="warning">
                        <Icon>bolt</Icon>
                      </CardIcon>
                      <h3 className={classes.cardTitle}>
                        dsadsadsa
                      </h3>
                    </CardHeader>
                    <CardBody >
                      Total Microjoules: {totalMicroJoulesSpectre}
                    </CardBody>
                  </Card>
                </GridItem>
              </Grid>
            </Card>
          </GridItem>

        </Grid>
      </CardBody>
    </Card>
  );
}
