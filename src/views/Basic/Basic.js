import React, { useState } from "react";
import Button from "components/CustomButtons/Button.js";
import { Button as MaterialButton } from '@material-ui/core/';
import Radio from "@material-ui/core/Radio";
import CircularProgress from '@material-ui/core/CircularProgress';
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";
import styles from "assets/jss/material-dashboard-react/checkboxAdnRadioStyle.js";
import { makeStyles } from "@material-ui/core/styles";
import axios from 'axios';

import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { API_HOST } from "utils/api";
import getCsrfToken from "utils/csrf";

const cardStyles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const cardUseStyles = makeStyles(cardStyles);

const useStyles = makeStyles(styles);

export default function Basic() {

  const [imageSelected, setImageSelected] = useState("");
  const [selectedManipulation, setSelectedManipulation] = useState("");
  const [isFileSelected, setIsFileSelected] = useState("");
  const [isManipulationSelected, setIsManipulationSelected] = useState("");
  const [disableButton, setDisableButton] = useState(true);
  const [isUploading, setIsUploading] = useState(false);
  const [isDownloadable, setIsDownloadable] = useState(false);
  const [downloadFile, setDownloadFile] = useState("")

  // Function to trigger download in browser
  const triggerDownload = response => {
    const file = new File([response.data], imageSelected.name);
    let url = window.URL.createObjectURL(file);
    let a = document.createElement('a');
    a.href = url;
    a.download = imageSelected.name;
    a.click();
  }

  var width, height; //to grab image dimensions

  const loadImage = () => { //load image from user and grab dimensions
    return new Promise((resolve) => {
      var reader = new FileReader();

      //Read the contents of Image File.
      reader.readAsDataURL(imageSelected);

      reader.onload = function (e) {
        //Initiate the JavaScript Image object.
        var image = new Image();

        //Set the Base64 string return from FileReader as source.
        image.src = e.target.result;

        image.onload = function () {
          height = this.height;
          width = this.width;
          resolve(image)
        };
      };
    })
  }

  const uploadImage = () => {
    setIsUploading(true);
    loadImage().then(async () => { //wait to fetch dimensions, before constructing post request
      // Construct body of post request
      const bodyFormData = new FormData();
      bodyFormData.append("size", imageSelected.size);
      bodyFormData.append("name", imageSelected.name);
      bodyFormData.append("type", imageSelected.type);
      bodyFormData.append("action", selectedManipulation);
      bodyFormData.append("dimensions", { width, height });
      bodyFormData.append("X-CSRFToken", await getCsrfToken());

      // Send image to master load balancer endpoint
      axios.post(`${API_HOST}/master/lb`, bodyFormData)
        .then(response => response.data)
        .then(uploadEndpoint => {
          // append image in body data for the new request to the actual worker
          bodyFormData.append("image-file", imageSelected);
          axios.post(`${uploadEndpoint}`, bodyFormData,
            { responseType: 'blob' })
            .then(response => {
              setIsUploading(false);
              setIsDownloadable(true);
              setDownloadFile(response);
              triggerDownload(response);
            })
        })
        .catch(error => {
          setIsUploading(false);
          setIsDownloadable(false); // Return user back to step 1
          console.log(error)
        });

    });
  }

  const classes = useStyles();
  const cardClasses = cardUseStyles();

  const uploadStep = (
    <Card>
      <CardHeader color="primary">
        <h4 className={cardClasses.cardTitleWhite}>Basic Image Manipulation</h4>
        <p className={cardClasses.cardCategoryWhite}>
          Manipulate an image with either Flip, Flop, or Grayscale
        </p>
      </CardHeader>
      <CardBody>
        <Grid container>
          <GridItem xs={12} sm={12} md={4}>
            <Radio
              checked={selectedManipulation === "flip"}
              onChange={() => {
                setSelectedManipulation("flip");
                setIsManipulationSelected('true');
                if (isFileSelected === 'true') {
                  setDisableButton(false)
                }
              }}
              value="flip"
              name="Flip"
              aria-label="A"
              icon={<FiberManualRecord className={classes.radioUnchecked} />}
              checkedIcon={<FiberManualRecord className={classes.radioChecked} />}
              classes={{
                checked: classes.radio
              }}
            />
            Flip
            <Radio
              checked={selectedManipulation === "flop"}
              onChange={() => {
                setSelectedManipulation("flop");
                setIsManipulationSelected('true');
                if (isFileSelected === 'true') {
                  setDisableButton(false)
                }
              }}
              value="flop"
              name="Flop"
              aria-label="B"
              icon={<FiberManualRecord className={classes.radioUnchecked} />}
              checkedIcon={<FiberManualRecord className={classes.radioChecked} />}
              classes={{
                checked: classes.radio
              }}
            />
            Flop
            <Radio
              checked={selectedManipulation === "grayscale"}
              onChange={() => {
                setSelectedManipulation("grayscale");
                setIsManipulationSelected('true');
                if (isFileSelected === 'true') {
                  setDisableButton(false)
                }
              }}
              value="grayscale"
              name="Grayscale"
              aria-label="C"
              icon={<FiberManualRecord className={classes.radioUnchecked} />}
              checkedIcon={<FiberManualRecord className={classes.radioChecked} />}
              classes={{
                checked: classes.radio
              }}
            />
            Grayscale
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <MaterialButton
              variant="contained"
              component="label"
            >
              Choose File
              <input
                hidden
                type="file"
                accept="image/jpg, image/png, image/gif, image/webp"
                multiple={false} //single files
                onChange={(event) => {
                  setImageSelected(event.target.files[0]);
                  setIsFileSelected('true');
                  if (isManipulationSelected === 'true') {
                    setDisableButton(false)
                  }
                }}
              />
            </MaterialButton>
            &nbsp;
            {isFileSelected && imageSelected.name}
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Button disabled={disableButton} color="primary" round onClick={uploadImage}>
              Upload Image
            </Button>
          </GridItem>
        </Grid>
      </CardBody>
    </Card>
  );

  const loadingStep = (
    <div className="center-text">
      <h2>Please wait...</h2>
      <div className="center-circle"><CircularProgress /></div>
    </div>
  );

  const manualDownload = () => {
    triggerDownload(downloadFile);
  }

  const resetScreen = () => {
    setIsUploading(false);
    setIsDownloadable(false);
    setDownloadFile("");
  }

  const downloadStep = (
    <div className="center-text">
      <h2>Download should begin automatically</h2>
      <MaterialButton variant="contained" color="primary" onClick={manualDownload}>
        Download Manually
      </MaterialButton>
      <br />
      <br />
      <MaterialButton variant="outlined" color="secondary" onClick={resetScreen}>
        Upload Another Image
      </MaterialButton>
    </div>
  );

  return (
    <div>
      {(!isUploading && !isDownloadable) && uploadStep}
      {isUploading && loadingStep}
      {isDownloadable && downloadStep}
    </div>
  );
}


